import Vue from 'vue'
import App from './App.vue'
//This is how we register global component
//Vue.component('text-input',{
//    template:'<textarea :placeholder="textValue" @click="textChange()"></textarea>',
//    data:function(){
//        return{
//            textValue:"TypeHere"
//        }
//    },
//    methods:{
//        textChange:function(){
//            this.textValue= "TextChanges";
//        }
//    }
//})
//this is how we register local component
//var component1={
//    template:'<h1>Component1</h1>'
//}
new Vue({
  el: '#app',
  components:{
//      'component-1':component1
  },
  render: h => h(App)
})
